﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SceneShifter
{
    public partial class frmDialogTweaker : Form
    {
        public frmDialogTweaker()
        {
            InitializeComponent();
        }

        public string fileName;


        private missionXML missionD;

        internal missionXML MissionD
        {
            get
            {
                return missionD;
            }

            set
            {
                missionD = value;
            }
        }

        private void frmDialogTweaker_Load(object sender, EventArgs e)
        {
            ShowTable();
        }

        private void frmDialogTweaker_FormClosed(object sender, EventArgs e)
        {
            MessageBox.Show("Badabumm");

            for (int i=0; i<dataDialogs.Rows.Count;i++)
            {
                string idString = "<SimMission.DialogAction InstanceId=\"" + dataDialogs[3, i].Value + "\"";
                string simMission = "<SimMission.DialogAction InstanceId=\"" + dataDialogs[3, i].Value + "\">" +
                    "<Descr>" + dataDialogs[2, i].Value + "</Descr>" +
                    "<SoundType>" + dataDialogs[4, i].Value + "</SoundType>" +
                    "<Text>" + dataDialogs[5, i].Value + "</Text>" +
                    "<SoundFileName>" + dataDialogs[6, i].Value + "</SoundFileName>" +
                    "<VoiceSelection>Default</VoiceSelection></SimMission.DialogAction>";

                for (int m=0; m< MissionD.ElementCount;m++)
                {
                    
                    if (missionD.txtMissions[m].IndexOf(idString) !=-1)
                    {
                        missionD.txtMissions[m] = simMission;

                    }

                }

            }

            frmSceneShifter newForm = new frmSceneShifter();
            newForm.mission1 = missionD;
            newForm.Tag = fileName;
            newForm.GetSettings();
            newForm.ShowTable();
            newForm.Show(); 

            
        }


        private void ShowTable()
        {
            DataTable myTable = new DataTable("MissionElements");

            myTable.Columns.Add("No");
            myTable.Columns.Add("Type");
            myTable.Columns.Add("Name");
            myTable.Columns.Add("InstanceID");
            myTable.Columns.Add("SoundType");
            myTable.Columns.Add("Text");
            myTable.Columns.Add("SoundFileName");


            for (int i = 0; i < (MissionD.ElementCount); i++)
            {
                simMission nextSimVar = new simMission(MissionD.txtMissions[i]);

                string SoundType="";
                string Text="";
                string SoundFileName="";
                for (int k = 0; k < nextSimVar.elements.Rows.Count; k++)
                {
                    if (Convert.ToString(nextSimVar.elements.Rows[k][0]) == "SoundType")
                    {
                        SoundType = Convert.ToString(nextSimVar.elements.Rows[k][1]);
                    }
                    if (Convert.ToString(nextSimVar.elements.Rows[k][0]) == "Text")
                    {
                        Text = Convert.ToString(nextSimVar.elements.Rows[k][1]);
                    }
                    if (Convert.ToString(nextSimVar.elements.Rows[k][0]) == "SoundFileName")
                    {
                        SoundFileName = Convert.ToString(nextSimVar.elements.Rows[k][1]);
                    }

                }

                myTable.Rows.Add(i, nextSimVar.Type, nextSimVar.Name, nextSimVar.InstanceID,SoundType,Text,SoundFileName);

                /*nextSimVar.elements.Rows[7][1];*/
            }



            DataView myView = new DataView(myTable);

            myView.RowFilter = "Type like '%Dialog%'";

            dataDialogs.DataSource = myView;
            dataDialogs.Columns[0].Width = 30;
            dataDialogs.Columns[1].Width = 70;
            dataDialogs.Columns[2].Width = 150;
            dataDialogs.Columns[3].Width = 240;
            dataDialogs.Columns[4].Width = 90;
            dataDialogs.Columns[5].Width = 320;
            dataDialogs.Columns[6].Width = 145;
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            ShowTable();
        }

        private void btnToSound_Click(object sender, EventArgs e)
        {
            for (int i=0; i<dataDialogs.Rows.Count;i++)
            {
                if(dataDialogs[0, i].Value != null)
                { 
                dataDialogs[ 4,i].Value = "Sound File";

                    if (dataDialogs[6,i].Value != null)
                    {

                        string soundFileName = txtSoundFolder.Text;

                        if (rbRowID.Checked==true)
                        {
                            soundFileName += txtSoundFileName.Text + dataDialogs[0, i].Value + ".wav";
                        }
                        else
                        {
                            soundFileName += txtSoundFileName.Text + (i+1) + ".wav";
                        }

                        dataDialogs[6, i].Value = soundFileName;
                    }

                }
            }


        }
    }
}
