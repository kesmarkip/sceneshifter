﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SceneShifter
{

    
   

   



    public partial class frmSceneShifter : Form
    {

       public missionXML mission1;

       
        public frmSceneShifter()
        {
            InitializeComponent();
            
        }

        public void ShowTable()
        {

            if(this.Tag != null)
            {
                lblFile.Text = Convert.ToString(this.Tag);
            }

            DataTable myTable = new DataTable("MissionElements");

            myTable.Columns.Add("No");
            myTable.Columns.Add("Type");
            myTable.Columns.Add("Name");
            myTable.Columns.Add("InstanceID");


            for (int i = 0; i < (mission1.ElementCount); i++)
            {
                simMission nextSimVar = new simMission(mission1.txtMissions[i]);
                myTable.Rows.Add(i, nextSimVar.Type, nextSimVar.Name, nextSimVar.InstanceID);

            }

            

            DataView myView = new DataView(myTable);

            myView.RowFilter = "Type like '%" + txtTypeFilter.Text + "%' and Name like '%" + txtNameFilter.Text + "%'";

            dataGrid.DataSource = myView;
            dataGrid.Columns[0].Width = 50;
            dataGrid.Columns[1].Width = 300;
            dataGrid.Columns[2].Width = 300;
            dataGrid.Columns[3].Width = 500;

        }


        public void GetSettings()
        {
            if (System.IO.File.Exists("mission.cfg") != false)
            {
                this.config = System.IO.File.ReadAllText("mission.cfg");

                string fileName;
                int lastPer = lblFile.Text.LastIndexOf("\\", StringComparison.Ordinal);
                if (lastPer == -1)
                {
                    lastPer++;
                    fileName = lblFile.Text;
                }
                else
                {
                    fileName = lblFile.Text.Substring(lastPer + 1, lblFile.Text.Length - lastPer - 1);
                }

                if (this.config.IndexOf(fileName, 0) != -1)
                {
                    int fromPos = this.config.IndexOf("<file=" + fileName, 0);
                    int toPos = this.config.IndexOf("</file>", fromPos + 7);
                    this.config_forfile = config.Substring(fromPos, toPos + 7);

                    DataTable varTable = new DataTable();
                    varTable.Columns.Add("VarName");
                    varTable.Columns.Add("VarValue");

                    int posID = 0;

                    while (posID < config_forfile.Length - 13)
                    {
                        int intVarStart = config_forfile.IndexOf("<var", posID);
                        if (intVarStart == -1) { break; }
                        int intVarEnd = config_forfile.IndexOf("</var>", intVarStart);
                        string varName = config_forfile.Substring(config_forfile.IndexOf("@", intVarStart), config_forfile.IndexOf(">", intVarStart) - config_forfile.IndexOf("@", intVarStart));
                        string varValue = config_forfile.Substring(config_forfile.IndexOf(">", intVarStart) + 1, config_forfile.IndexOf("</var>", intVarStart) - 1 - config_forfile.IndexOf(">", intVarStart));
                        varTable.Rows.Add(varName, varValue);
                        posID = intVarEnd + 6;

                    }

                    config_datatable = varTable.Copy();
                    dataGridVar.DataSource = new DataView(varTable);
                    dataGridVar.Columns[0].Width = 200;
                    dataGridVar.Columns[1].Width = 550;
                }
            }
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {

            this.filetext = System.IO.File.ReadAllText(lblFile.Text);

            GetSettings();

            mission1 = new missionXML(this.filetext);
            string mystring = Convert.ToString(mission1.ElementCount);

            /*
            System.Windows.Forms.MessageBox.Show(mystring);
            */
            ShowTable();



            
        }
     
   
     

        private void dataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int myRowID = Convert.ToInt32(dataGrid.CurrentRow.Cells[0].Value);

            simMission myElement = new simMission(mission1.txtMissions[myRowID]);

            dataGridDetails.Tag = myRowID;

            DataTable myTable = myElement.elements;
            DataView myView = new DataView(myTable);

            this.dataGridDetails.DataSource = myView;
            dataGridDetails.Columns[0].Width = 200;
            dataGridDetails.Columns[1].Width = 600;

            }

        private void dataGridDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            string newMissiontext = "<SimMission." + dataGridDetails.Rows[0].Cells[1].Value + " InstanceId=\"" + dataGridDetails.Rows[2].Cells[1].Value + "\">\r\n";
            newMissiontext += "<Descr>" + dataGridDetails.Rows[1].Cells[1].Value + "</Descr>\r\n";

            for (int i=3; i<dataGridDetails.Rows.Count ;i++)
            {
                newMissiontext += "<" + dataGridDetails.Rows[i].Cells[0].Value + ">" + dataGridDetails.Rows[i].Cells[1].Value + "</" + dataGridDetails.Rows[i].Cells[0].Value + ">\r\n";
            }

            newMissiontext += "</SimMission." + dataGridDetails.Rows[0].Cells[1].Value + ">\r\n";

            mission1.txtMissions[Convert.ToInt16(dataGridDetails.Tag)] = newMissiontext;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            SaveFileDialog mySaveDialog = new SaveFileDialog();

            mySaveDialog.Title = "Export SimDirector Group Files";
            mySaveDialog.CheckFileExists = false;
            mySaveDialog.CheckPathExists = true;
            mySaveDialog.DefaultExt = "ogrp";
            mySaveDialog.Filter = "SimDirector Group files (*.ogrp)|*.ogrp|All files (*.*)|*.*";
            mySaveDialog.FilterIndex = 1;
            mySaveDialog.RestoreDirectory = true;
            mySaveDialog.ShowDialog();

            string exportName = mySaveDialog.FileName;

            System.IO.File.WriteAllText(exportName, mission1.FullString);
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void txtTypeFilter_TextChanged(object sender, EventArgs e)
        {
            ShowTable();
        }

        private void txtNameFilter_TextChanged(object sender, EventArgs e)
        {
            ShowTable();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if( dataGridVar.Rows.Count!=0)
            {
                for (int i=0;i<dataGridVar.Rows.Count;i++)
                {

                    
                    if (dataGridVar.Rows[i].Cells[1].Value != config_datatable.Rows[i][1])
                    {

                        for (int j=0;j<mission1.txtMissions.Count(); j++)
                        {
                            mission1.txtMissions[j] = mission1.txtMissions[j].Replace(Convert.ToString(config_datatable.Rows[i][1]), Convert.ToString(dataGridVar.Rows[i].Cells[1].Value));
                        }



                    }

                    ShowTable();


                }


            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog myFile = new OpenFileDialog();
            myFile.ShowDialog();

            lblFile.Text = myFile.FileName;
            btnReadFile_Click(lblFile, null);


        }

        private void btnShowDialog_Click(object sender, EventArgs e)
        {
            
            frmDialogTweaker myDialog = new frmDialogTweaker();
            myDialog.fileName = lblFile.Text;
           myDialog.MissionD = mission1;
           myDialog.ShowDialog();
           this.Hide();
        }

        private void frmSceneShifter_FormClosed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string DrawStyle = "";
            if (rbDraw_Filled.Checked == true)
            {
                DrawStyle = "Filled";
            }
            if (rbDraw_Outlined.Checked == true)
            {
                DrawStyle = "Outlined";
            }
            if (rbDraw_None.Checked == true)
            {
                DrawStyle = "None";
            }





            for (int j = 0; j < mission1.txtMissions.Count(); j++)
                        {
                            mission1.txtMissions[j] = mission1.txtMissions[j].Replace("<DrawStyle>Filled</DrawStyle>", "<DrawStyle>" + DrawStyle + "</DrawStyle>");
                            mission1.txtMissions[j] = mission1.txtMissions[j].Replace("<DrawStyle>Outlined</DrawStyle>", "<DrawStyle>" + DrawStyle + "</DrawStyle>");
                             mission1.txtMissions[j] = mission1.txtMissions[j].Replace("<DrawStyle>None</DrawStyle>", "<DrawStyle>" + DrawStyle + "</DrawStyle>");
                         }




                    ShowTable();
                }
    }
}


