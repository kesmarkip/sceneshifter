﻿namespace SceneShifter
{
    partial class frmDialogTweaker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataDialogs = new System.Windows.Forms.DataGridView();
            this.btnReload = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoundFolder = new System.Windows.Forms.TextBox();
            this.btnToSound = new System.Windows.Forms.Button();
            this.rbManual = new System.Windows.Forms.RadioButton();
            this.rbRowID = new System.Windows.Forms.RadioButton();
            this.rbIncrID = new System.Windows.Forms.RadioButton();
            this.txtSoundFileName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataDialogs)).BeginInit();
            this.SuspendLayout();
            // 
            // dataDialogs
            // 
            this.dataDialogs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataDialogs.Location = new System.Drawing.Point(12, 52);
            this.dataDialogs.Name = "dataDialogs";
            this.dataDialogs.Size = new System.Drawing.Size(1091, 451);
            this.dataDialogs.TabIndex = 0;
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(12, 12);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(110, 29);
            this.btnReload.TabIndex = 1;
            this.btnReload.Text = "Reload table";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(851, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sound folder";
            // 
            // txtSoundFolder
            // 
            this.txtSoundFolder.Location = new System.Drawing.Point(924, 17);
            this.txtSoundFolder.Name = "txtSoundFolder";
            this.txtSoundFolder.Size = new System.Drawing.Size(179, 20);
            this.txtSoundFolder.TabIndex = 3;
            this.txtSoundFolder.Text = "Sounds\\";
            // 
            // btnToSound
            // 
            this.btnToSound.Location = new System.Drawing.Point(128, 12);
            this.btnToSound.Name = "btnToSound";
            this.btnToSound.Size = new System.Drawing.Size(110, 29);
            this.btnToSound.TabIndex = 4;
            this.btnToSound.Text = "Text to Sound";
            this.btnToSound.UseVisualStyleBackColor = true;
            this.btnToSound.Click += new System.EventHandler(this.btnToSound_Click);
            // 
            // rbManual
            // 
            this.rbManual.AutoSize = true;
            this.rbManual.Location = new System.Drawing.Point(272, 18);
            this.rbManual.Name = "rbManual";
            this.rbManual.Size = new System.Drawing.Size(97, 17);
            this.rbManual.TabIndex = 5;
            this.rbManual.Text = "Manual naming";
            this.rbManual.UseVisualStyleBackColor = true;
            // 
            // rbRowID
            // 
            this.rbRowID.AutoSize = true;
            this.rbRowID.Checked = true;
            this.rbRowID.Location = new System.Drawing.Point(375, 18);
            this.rbRowID.Name = "rbRowID";
            this.rbRowID.Size = new System.Drawing.Size(86, 17);
            this.rbRowID.TabIndex = 6;
            this.rbRowID.TabStop = true;
            this.rbRowID.Text = "ID by RowID";
            this.rbRowID.UseVisualStyleBackColor = true;
            // 
            // rbIncrID
            // 
            this.rbIncrID.AutoSize = true;
            this.rbIncrID.Location = new System.Drawing.Point(467, 18);
            this.rbIncrID.Name = "rbIncrID";
            this.rbIncrID.Size = new System.Drawing.Size(94, 17);
            this.rbIncrID.TabIndex = 7;
            this.rbIncrID.Text = "Incremental ID";
            this.rbIncrID.UseVisualStyleBackColor = true;
            // 
            // txtSoundFileName
            // 
            this.txtSoundFileName.Location = new System.Drawing.Point(761, 17);
            this.txtSoundFileName.Name = "txtSoundFileName";
            this.txtSoundFileName.Size = new System.Drawing.Size(84, 20);
            this.txtSoundFileName.TabIndex = 9;
            this.txtSoundFileName.Text = "dialog";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(675, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Sound filename";
            // 
            // frmDialogTweaker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 515);
            this.Controls.Add(this.txtSoundFileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbIncrID);
            this.Controls.Add(this.rbRowID);
            this.Controls.Add(this.rbManual);
            this.Controls.Add(this.btnToSound);
            this.Controls.Add(this.txtSoundFolder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.dataDialogs);
            this.Name = "frmDialogTweaker";
            this.Text = "Dialog Tweaker";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmDialogTweaker_FormClosed);
            this.Load += new System.EventHandler(this.frmDialogTweaker_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataDialogs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataDialogs;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSoundFolder;
        private System.Windows.Forms.Button btnToSound;
        private System.Windows.Forms.RadioButton rbManual;
        private System.Windows.Forms.RadioButton rbRowID;
        private System.Windows.Forms.RadioButton rbIncrID;
        private System.Windows.Forms.TextBox txtSoundFileName;
        private System.Windows.Forms.Label label2;
    }
}