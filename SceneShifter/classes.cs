﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SceneShifter
{
    
   public class missionXML
    {

        public simMission[] missionElements;
        public string[] txtMissions;
        private string header;
        private string footer;
        
        public string txtMission(int MissionID)
        {
           return txtMissions[MissionID];
        }  
          

        public string FullString
        {
            get
            {
                string tmpString;

                tmpString = header;

                for (int i=0; i < txtMissions.Length; i++)
                {
                    tmpString += txtMissions[i];
                }

                tmpString += footer;

                return tmpString;
                }
        }

        public int ElementCount
        {
            get
            {
                return missionElements.Length;
            }
        }

       
        public missionXML(string xmlfile)
        {

            int mElementCount = 0;
            int startPos = 0;

            /*Checking the count of the elements*/

            int firstElementStart = xmlfile.IndexOf("<SimMission.", startPos);

            header = xmlfile.Substring(0, firstElementStart - 1);

            while (3 > 1)
            {

                firstElementStart = xmlfile.IndexOf("<SimMission.", startPos);

                int firstElementEnd = xmlfile.IndexOf("</SimMission.", startPos);
                int firstElementEndClose = xmlfile.IndexOf(">", firstElementEnd);

                Console.WriteLine("ElementStart:" + firstElementStart);
                Console.WriteLine("ElementEnd:" + firstElementEnd);
                Console.WriteLine("ElementEndClose:" + firstElementEndClose);


                if (firstElementStart != -1)
                {

                    mElementCount++;
                    startPos = firstElementStart + 1;
                }
                else
                {
                    break;
                }

            }

            startPos = 0;
            missionElements = new simMission[mElementCount];
            txtMissions = new string[mElementCount];
                     
            for (int i = 0; i < mElementCount; i++)
            {

                firstElementStart = xmlfile.IndexOf("<SimMission.", startPos);
                int firstElementEnd = xmlfile.IndexOf("</SimMission.", startPos);
                int firstElementEndClose = xmlfile.IndexOf(">", firstElementEnd);

                string mElement = xmlfile.Substring(firstElementStart, (firstElementEndClose - firstElementStart + 1));

                simMission myNewElement;
                myNewElement = new simMission(mElement);
            
                missionElements[i] = myNewElement;
                txtMissions[i] = mElement;

                startPos = firstElementEndClose;
                footer = xmlfile.Substring(firstElementEndClose+1, xmlfile.Length - firstElementEndClose-1);
            }

           




        }





    }


    public class simMission
    {
        public string Type;
        public string InstanceID;
        public string Name;
        static string text;
        private DataTable myElements;

        public DataTable elements
        {
            get
            {
                return myElements;
            }
            set
            {
                myElements = elements;
            }
        }

        public string FullText
        {
            get
            {
                return text;
            }
                             
            }

        public simMission(string missionConstructor)
        {
            text = missionConstructor;

            string startingID = "<SimMission.";
            string instanceString = "InstanceId=";


            int a, b, c, d, e, f;
            int h;
            a = missionConstructor.IndexOf(startingID, 0) ;
            b = a + startingID.Length;
            c = missionConstructor.IndexOf(" ", b);
            Type = missionConstructor.Substring(b, c - b);

            d = missionConstructor.IndexOf(instanceString,c) + instanceString.Length + 1;
            e = missionConstructor.IndexOf("\">", d)+2;
            InstanceID = missionConstructor.Substring(d, e - d -2);
            f = missionConstructor.IndexOf("<Descr>", e)+7;
            h = missionConstructor.IndexOf("</Descr>", f);
            Name = missionConstructor.Substring(f, h - f);

            DataTable DataElements = new DataTable();
            DataElements.Columns.Add("Name");
            DataElements.Columns.Add("Value");
            DataElements.Rows.Add("Type", Type);
            DataElements.Rows.Add("Name", Name);
            DataElements.Rows.Add("InstanceID", InstanceID);

            int closeToEnd = missionConstructor.IndexOf("</SimMission.", 0) - 13;

            int posMarker = h+7;

            while (posMarker<closeToEnd)
            {
                int fromID = missionConstructor.IndexOf("<",posMarker);
                int toID = missionConstructor.IndexOf(">", fromID);
                string elementName = missionConstructor.Substring(fromID + 1, toID - fromID-1);

                int endID = 0;
                if (elementName.Length >16 && elementName.Substring(0,16)=="ObjectReference ")
                {
                    endID = missionConstructor.IndexOf("</ObjectReference>",fromID);
                }
                else
                {
                    endID = missionConstructor.IndexOf("</" + elementName + ">",fromID);
                }

               
                string elementValue = missionConstructor.Substring(toID + 1, endID - toID-1);
                DataElements.Rows.Add(elementName, elementValue);
                posMarker = endID + elementName.Length+3;
            }


            myElements = DataElements;

            
        }

    }

}
