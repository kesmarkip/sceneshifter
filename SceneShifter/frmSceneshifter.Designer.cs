﻿using System.Data;

namespace SceneShifter
{
   
    partial class frmSceneShifter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFileCaption = new System.Windows.Forms.Label();
            this.lblFile = new System.Windows.Forms.Label();
            this.btnSelect = new System.Windows.Forms.Button();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.lblC1 = new System.Windows.Forms.Label();
            this.dataGridDetails = new System.Windows.Forms.DataGridView();
            this.lblC2 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtNameFilter = new System.Windows.Forms.TextBox();
            this.txtTypeFilter = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.rbDraw_Filled = new System.Windows.Forms.RadioButton();
            this.rbDraw_Outlined = new System.Windows.Forms.RadioButton();
            this.rbDraw_None = new System.Windows.Forms.RadioButton();
            this.btnShowDialog = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.dataGridVar = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDetails)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVar)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFileCaption
            // 
            this.lblFileCaption.AutoSize = true;
            this.lblFileCaption.Location = new System.Drawing.Point(248, 18);
            this.lblFileCaption.Name = "lblFileCaption";
            this.lblFileCaption.Size = new System.Drawing.Size(68, 13);
            this.lblFileCaption.TabIndex = 0;
            this.lblFileCaption.Text = "Scenario file:";
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Location = new System.Drawing.Point(322, 18);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(0, 13);
            this.lblFile.TabIndex = 1;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(23, 14);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(68, 21);
            this.btnSelect.TabIndex = 3;
            this.btnSelect.Text = "Select file";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // dataGrid
            // 
            this.dataGrid.AllowUserToAddRows = false;
            this.dataGrid.AllowUserToDeleteRows = false;
            this.dataGrid.AllowUserToOrderColumns = true;
            this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid.Location = new System.Drawing.Point(19, 325);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.ReadOnly = true;
            this.dataGrid.Size = new System.Drawing.Size(1160, 178);
            this.dataGrid.TabIndex = 7;
            this.dataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_CellContentClick);
            // 
            // lblC1
            // 
            this.lblC1.AutoSize = true;
            this.lblC1.Location = new System.Drawing.Point(19, 309);
            this.lblC1.Name = "lblC1";
            this.lblC1.Size = new System.Drawing.Size(85, 13);
            this.lblC1.TabIndex = 8;
            this.lblC1.Text = "MissionElements";
            // 
            // dataGridDetails
            // 
            this.dataGridDetails.AllowUserToAddRows = false;
            this.dataGridDetails.AllowUserToDeleteRows = false;
            this.dataGridDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridDetails.Location = new System.Drawing.Point(19, 526);
            this.dataGridDetails.Name = "dataGridDetails";
            this.dataGridDetails.Size = new System.Drawing.Size(1160, 206);
            this.dataGridDetails.TabIndex = 9;
            this.dataGridDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridDetails_CellEndEdit);
            // 
            // lblC2
            // 
            this.lblC2.AutoSize = true;
            this.lblC2.Location = new System.Drawing.Point(19, 510);
            this.lblC2.Name = "lblC2";
            this.lblC2.Size = new System.Drawing.Size(121, 13);
            this.lblC2.TabIndex = 10;
            this.lblC2.Text = "MissionElement - Details";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(97, 14);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(68, 21);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Export file";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtNameFilter
            // 
            this.txtNameFilter.Location = new System.Drawing.Point(406, 299);
            this.txtNameFilter.Name = "txtNameFilter";
            this.txtNameFilter.Size = new System.Drawing.Size(86, 20);
            this.txtNameFilter.TabIndex = 12;
            this.txtNameFilter.TextChanged += new System.EventHandler(this.txtNameFilter_TextChanged);
            // 
            // txtTypeFilter
            // 
            this.txtTypeFilter.Location = new System.Drawing.Point(314, 299);
            this.txtTypeFilter.Name = "txtTypeFilter";
            this.txtTypeFilter.Size = new System.Drawing.Size(86, 20);
            this.txtTypeFilter.TabIndex = 13;
            this.txtTypeFilter.TextChanged += new System.EventHandler(this.txtTypeFilter_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnShowDialog);
            this.groupBox1.Controls.Add(this.btnApply);
            this.groupBox1.Controls.Add(this.dataGridVar);
            this.groupBox1.Location = new System.Drawing.Point(12, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1176, 254);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Predefined variables";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.rbDraw_Filled);
            this.groupBox2.Controls.Add(this.rbDraw_Outlined);
            this.groupBox2.Controls.Add(this.rbDraw_None);
            this.groupBox2.Location = new System.Drawing.Point(1067, 87);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(99, 158);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Draw Style";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 113);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 39);
            this.button1.TabIndex = 3;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rbDraw_Filled
            // 
            this.rbDraw_Filled.AutoSize = true;
            this.rbDraw_Filled.Location = new System.Drawing.Point(8, 75);
            this.rbDraw_Filled.Name = "rbDraw_Filled";
            this.rbDraw_Filled.Size = new System.Drawing.Size(49, 17);
            this.rbDraw_Filled.TabIndex = 2;
            this.rbDraw_Filled.Text = "Filled";
            this.rbDraw_Filled.UseVisualStyleBackColor = true;
            // 
            // rbDraw_Outlined
            // 
            this.rbDraw_Outlined.AutoSize = true;
            this.rbDraw_Outlined.Location = new System.Drawing.Point(8, 52);
            this.rbDraw_Outlined.Name = "rbDraw_Outlined";
            this.rbDraw_Outlined.Size = new System.Drawing.Size(64, 17);
            this.rbDraw_Outlined.TabIndex = 1;
            this.rbDraw_Outlined.Text = "Outlined";
            this.rbDraw_Outlined.UseVisualStyleBackColor = true;
            // 
            // rbDraw_None
            // 
            this.rbDraw_None.AutoSize = true;
            this.rbDraw_None.Checked = true;
            this.rbDraw_None.Location = new System.Drawing.Point(8, 29);
            this.rbDraw_None.Name = "rbDraw_None";
            this.rbDraw_None.Size = new System.Drawing.Size(51, 17);
            this.rbDraw_None.TabIndex = 0;
            this.rbDraw_None.TabStop = true;
            this.rbDraw_None.Text = "None";
            this.rbDraw_None.UseVisualStyleBackColor = true;
            // 
            // btnShowDialog
            // 
            this.btnShowDialog.Location = new System.Drawing.Point(1059, 19);
            this.btnShowDialog.Name = "btnShowDialog";
            this.btnShowDialog.Size = new System.Drawing.Size(108, 44);
            this.btnShowDialog.TabIndex = 2;
            this.btnShowDialog.Text = "Dialog tweaker";
            this.btnShowDialog.UseVisualStyleBackColor = true;
            this.btnShowDialog.Click += new System.EventHandler(this.btnShowDialog_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(855, 19);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(131, 21);
            this.btnApply.TabIndex = 1;
            this.btnApply.Text = "Apply changes";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // dataGridVar
            // 
            this.dataGridVar.AllowUserToAddRows = false;
            this.dataGridVar.AllowUserToDeleteRows = false;
            this.dataGridVar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridVar.Location = new System.Drawing.Point(7, 19);
            this.dataGridVar.Name = "dataGridVar";
            this.dataGridVar.Size = new System.Drawing.Size(842, 227);
            this.dataGridVar.TabIndex = 0;
            // 
            // frmSceneShifter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 742);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtTypeFilter);
            this.Controls.Add(this.txtNameFilter);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblC2);
            this.Controls.Add(this.dataGridDetails);
            this.Controls.Add(this.lblC1);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.lblFile);
            this.Controls.Add(this.lblFileCaption);
            this.Name = "frmSceneShifter";
            this.Text = "Scenario Group Tweaker by Propair Flight";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmSceneShifter_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDetails)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFileCaption;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.Button btnSelect;
        private string filetext;
        private string config;
        private string config_forfile;
        private DataTable config_datatable;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.Label lblC1;
        private System.Windows.Forms.DataGridView dataGridDetails;
        private System.Windows.Forms.Label lblC2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtNameFilter;
        private System.Windows.Forms.TextBox txtTypeFilter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridVar;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnShowDialog;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton rbDraw_Filled;
        private System.Windows.Forms.RadioButton rbDraw_Outlined;
        private System.Windows.Forms.RadioButton rbDraw_None;
    }
}

